const cars = require('./cars/cars')
const yearsOfCars = require('./problem4')

let inventory = cars.inventory

const carsOlder = (inventory)=>{

    if(inventory===undefined) return 
    if (!Array.isArray(inventory) ) return;
    if(inventory.length === 0 ) return

let years = yearsOfCars(inventory)
let olderCars = []

for (i= 0 ; i < inventory.length ; i ++ ){
    if (years[i] < 2000){
olderCars.push(inventory[i])
    }
}

return olderCars
}


console.log(carsOlder(inventory))


module.exports = carsOlder