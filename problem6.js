const cars = require('./cars/cars')

const inventory = cars.inventory 


const carsOfBMWAndAudi = (inventory) => {
    let BMWAndAudiCars = []

    if(inventory===undefined) return 
    if (!Array.isArray(inventory) ) return;
    if(inventory.length === 0 ) return

    for (i = 0; i< inventory.length; i++){
        if(inventory[i].car_make == "BMW" || inventory[i].car_make == "Audi"){
            BMWAndAudiCars.push(inventory[i])
        }
    }

    return BMWAndAudiCars
}

console.log(carsOfBMWAndAudi(inventory))

module.exports = carsOfBMWAndAudi