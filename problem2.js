const cars = require("./cars/cars")
const inventory = cars.inventory 




function findLastCar(inventory){
    if(inventory===undefined) return 
    if (!Array.isArray(inventory) ) return;
    if(inventory.length === 0 ) return
  
   let lastCar = inventory[inventory.length - 1]

    return `Last car is a ${lastCar.car_make} ${lastCar.car_model}`
}

module.exports = findLastCar

